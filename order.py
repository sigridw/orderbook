#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Sep 24 21:22:02 2019

@author: liqiuling
"""
import json
from websocket import create_connection
import asyncio


def updateBook(symbol):
    ws = create_connection("wss://api-pub.bitfinex.com/ws/2")
    # tBTCUSD
    # EOSBTC
    # STJBTC  STORJ/BTC
    ws.send(json.dumps({
        "event": "subscribe",
        "channel": "book",
        "symbol": symbol,
        "prec": "P0"
    }))
    
    ws.send(json.dumps({
            "event":"conf",
            "flags": 131072
            }
            ))

    while True:
        result = ws.recv()
        result = json.loads(result)
        
        ###########   build book based on snapshot   #############
        number = 0
        if type(result) is list and len(result[1]) > 10:
            #orderbook = result[1]
            bids = {
                       str(level[0]): [str(level[1]), str(level[2])]
                       for level in result[1] if level[2] > 0
            }
    
            asks = {
                       str(level[0]): [str(level[1]), str(level[2])[1:]]
                       for level in result[1] if level[2] < 0
            }
            
        ####### update orderbook     ##############
        if type(result) is list and len(result[1]) == 3:
            if result[1][1] == 0:
                if result[1][2] < 0:
                    #print('asks')
                    if result[1][0] in asks.keys():
                        del asks[result[1][0]]
                else:
                    if result[1][0] in bids.keys():
                        del bids[result[1][0]]
                    #print('bids')
            else:
                if result[1][2] < 0:
                    asks[result[1][0]] = [str(result[1][1]),str(result[1][2])[1:]]
                    #print('asks')
                else:
                    bids[result[1][0]] = [str(result[1][1]),str(result[1][2])]
                    #print('bids')
                
        #### print a new orderbook      #############
        if type(result) is list and result[1] == 'cs':
            number += 1
            print('print signal',result)
            print('number',number,'bids',bids)
            print('number',number,'asks',asks)
        #print('aaaa',type((result)))  
        
        print ("Received '%s'" % result)

    ws.close()

async def main():
    divs1 = loop.create_task(updateBook('tBTCUSD'))
    divs2 = loop.create_task(updateBook('EOSBTC'))
    divs3 = loop.create_task(updateBook('STJBTC'))
    await asyncio.wait([divs1, divs2, divs3])


if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main())
    loop.close()
    

